package com.yasitha.inteliment.view;


import android.app.Fragment;
import android.app.FragmentManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.yasitha.inteliment.R;
import com.yasitha.inteliment.databinding.FragmentTest1Binding;
import com.yasitha.inteliment.util.RecyclerItemClickListener;
import com.yasitha.inteliment.view.adapter.TopListAdapter;

/**
 * Created by Yasitha on 6/28/16.
 */
public class Test1Fragment extends Fragment implements View.OnClickListener {

    private static final String KEY_COLOR = "color";
    private static final String KEY_SELECTED_LIST_ITEM = "listitem";

    private String selectedListItemText;

    private FragmentTest1Binding fragmentTest1Binding;
    private int selectedColor;

    public Test1Fragment() {

    }

    public static Test1Fragment newInstance() {
        Test1Fragment fragment = new Test1Fragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_COLOR, selectedColor);
        outState.putString(KEY_SELECTED_LIST_ITEM, selectedListItemText);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            selectedColor = savedInstanceState.getInt(KEY_COLOR);
            if (selectedColor != 0)
                setColor(selectedColor);
            selectedListItemText = savedInstanceState.getString(KEY_SELECTED_LIST_ITEM);
            fragmentTest1Binding.selectedItemTv.setText(selectedListItemText);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        fragmentTest1Binding = DataBindingUtil.inflate(inflater, R.layout.fragment_test1, container, false);

        initView();

        return fragmentTest1Binding.getRoot();
    }

    void initView() {

        fragmentTest1Binding.redBtn.setOnClickListener(this);
        fragmentTest1Binding.greenBtn.setOnClickListener(this);
        fragmentTest1Binding.blueBtn.setOnClickListener(this);

        fragmentTest1Binding.section1RecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        fragmentTest1Binding.section1RecyclerView.setLayoutManager(linearLayoutManager);

        fragmentTest1Binding.section1RecyclerView.setAdapter(new TopListAdapter());

        fragmentTest1Binding.section1RecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                selectedListItemText = "Item " + (position + 1);
                fragmentTest1Binding.selectedItemTv.setText(selectedListItemText);
            }

        }));

        fragmentTest1Binding.pager.setAdapter(new PagerAdapter(getFragmentManager()));


    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        selectedColor = android.R.color.holo_red_dark;

        switch (id) {
            case R.id.redBtn:
                selectedColor = R.color.bgRed;
                break;
            case R.id.blueBtn:
                selectedColor = R.color.bgBlue;
                break;
            case R.id.greenBtn:
                selectedColor = R.color.bgGreen;
                break;
        }

        setColor(selectedColor);
    }

    private void setColor(int color) {
        fragmentTest1Binding.getRoot().setBackgroundColor(ContextCompat.getColor(getActivity(), color));
    }

    class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            PageFragment pageFragment = PageFragment.newInstance(position + 1);
            return pageFragment;
        }

        @Override
        public int getCount() {
            return 10;
        }
    }


}
