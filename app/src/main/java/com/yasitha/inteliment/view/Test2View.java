package com.yasitha.inteliment.view;

import com.yasitha.inteliment.model.domain.Attraction;

import java.util.List;

/**
 * Created by Yasitha on 6/28/16.
 */
public interface Test2View {

    void onLoadAttractions(List<Attraction> attractions);

    void onLoadAttractionsError();

}
