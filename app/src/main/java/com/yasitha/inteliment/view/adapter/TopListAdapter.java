package com.yasitha.inteliment.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yasitha.inteliment.R;
import com.yasitha.inteliment.databinding.TopListItemViewBinding;

/**
 * Created by Yasitha on 6/28/16.
 */
public class TopListAdapter extends RecyclerView.Adapter<TopListAdapter.TopListViewHolder> {


    @Override
    public TopListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        TopListItemViewBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.top_list_item_view, parent, false);
        TopListViewHolder topListViewHolder = new TopListViewHolder(binding);

        return topListViewHolder;
    }

    @Override
    public void onBindViewHolder(TopListViewHolder holder, int position) {
        holder.binding.itemTv.setText("Item " + (position + 1));
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public static class TopListViewHolder extends RecyclerView.ViewHolder {

        private TopListItemViewBinding binding;

        public TopListViewHolder(TopListItemViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


}
