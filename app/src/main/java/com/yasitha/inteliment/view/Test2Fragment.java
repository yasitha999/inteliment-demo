package com.yasitha.inteliment.view;


import android.app.Fragment;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.yasitha.inteliment.R;
import com.yasitha.inteliment.databinding.FragmentTest2Binding;
import com.yasitha.inteliment.model.domain.Attraction;
import com.yasitha.inteliment.presenter.Test2Presenter;
import com.yasitha.inteliment.presenter.Test2PresenterImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Yasitha on 6/28/16.
 */
public class Test2Fragment extends Fragment implements Test2View, AdapterView.OnItemSelectedListener, View.OnClickListener {

    private Test2Presenter presenter = new Test2PresenterImpl();

    private ArrayAdapter<Attraction> attractionArrayAdapter;

    private FragmentTest2Binding binding;

    private ArrayList<Attraction> attractions;
    private int selectedAttractionIndex = -1;

    public Test2Fragment() {
        presenter.setView(this);
    }

    public static Test2Fragment newInstance() {
        Test2Fragment fragment = new Test2Fragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_test2, container, false);
        View view = binding.getRoot();

        binding.attractionsSpinner.setOnItemSelectedListener(this);
        binding.navigateBtn.setOnClickListener(this);

        if (savedInstanceState == null) {
            presenter.getAttractions();
        } else {
            onLoadAttractions(attractions);
            binding.attractionsSpinner.setSelection(selectedAttractionIndex + 1);
        }

        return view;
    }

    @Override
    public void onLoadAttractions(List<Attraction> attractions) {

        this.attractions = (ArrayList<Attraction>) attractions;
        updateAttractionsAdapter(attractions);

    }

    private void updateAttractionsAdapter(List<Attraction> attractions) {

        if (attractions != null && attractions.size() > 0) {
            attractionArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, attractions);
            binding.attractionsSpinner.setAdapter(attractionArrayAdapter);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        this.selectedAttractionIndex = position;
        binding.transportInfoTv.setText("");

        System.out.println(position);
        if (position >= 0) {
            Attraction selectedAttraction = attractionArrayAdapter.getItem(position);
            populateAttraction(selectedAttraction);
        }
    }

    private void populateAttraction(Attraction attraction) {
        if (attraction != null) {
            Map<String, String> distanceParams = attraction.getDistanceFromCentral();

            for (Map.Entry<String, String> entry : distanceParams.entrySet()) {

                binding.transportInfoTv.append("\n" + entry.getKey() + " : " + entry.getValue());
            }
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onLoadAttractionsError() {
        Toast.makeText(getActivity(), R.string.error_attractions_load, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        if (selectedAttractionIndex >= 0 && attractions != null) {
            Attraction attraction = attractions.get(selectedAttractionIndex);

            Intent intent = new Intent(getActivity(), MapsActivity.class);
            intent.putExtra(MapsActivity.EXTRA_LAT, attraction.getAttractionLocation().getLatitude());
            intent.putExtra(MapsActivity.EXTRA_LNG, attraction.getAttractionLocation().getLongitude());
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), R.string.error_no_attraction, Toast.LENGTH_LONG).show();
        }
    }
}
