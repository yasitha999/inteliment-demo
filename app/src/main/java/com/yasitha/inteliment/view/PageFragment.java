package com.yasitha.inteliment.view;


import android.app.Fragment;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.yasitha.inteliment.R;
import com.yasitha.inteliment.databinding.FragmentPageBinding;

/**
 * Created by Yasitha on 6/28/16.
 */
public class PageFragment extends Fragment implements View.OnClickListener {

    private FragmentPageBinding fragmentPageBinding;

    private int index;

    public PageFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        index = getArguments().getInt("index");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentPageBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_page, container, false);

        fragmentPageBinding.fragmentTv.setText("Fragment : " + index);
        fragmentPageBinding.getRoot().setOnClickListener(this);

        return fragmentPageBinding.getRoot();
    }

    public static PageFragment newInstance(int index) {
        PageFragment fragment = new PageFragment();
        Bundle args = new Bundle();

        args.putInt("index", index);

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onClick(View v) {
        Toast.makeText(getActivity(), "Fragment Number - " + index, Toast.LENGTH_LONG).show();
    }
}
