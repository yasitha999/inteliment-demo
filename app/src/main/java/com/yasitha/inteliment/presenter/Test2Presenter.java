package com.yasitha.inteliment.presenter;

import com.yasitha.inteliment.view.Test2View;

/**
 * Created by Yasitha on 6/28/16.
 */
public interface Test2Presenter {

    void setView(Test2View view);

    void getAttractions();

}
