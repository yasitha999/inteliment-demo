package com.yasitha.inteliment.presenter;

import com.yasitha.inteliment.model.AttractionsModel;
import com.yasitha.inteliment.model.domain.Attraction;
import com.yasitha.inteliment.view.Test2View;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Yasitha on 6/28/16.
 */
public class Test2PresenterImpl implements Test2Presenter {

    private Test2View view;
    private AttractionsModel attractionsModel;

    public Test2PresenterImpl() {
        attractionsModel = new AttractionsModel();
    }

    public Test2PresenterImpl(Test2View view, AttractionsModel attractionsModel) {
        this.view = view;
        this.attractionsModel = attractionsModel;
    }

    @Override
    public void setView(Test2View view) {
        this.view = view;
    }

    @Override
    public void getAttractions() {

        Call<List<Attraction>> call = attractionsModel.getAttractions();
        call.enqueue(new Callback<List<Attraction>>() {
            @Override
            public void onResponse(Call<List<Attraction>> call, Response<List<Attraction>> response) {
                view.onLoadAttractions(response.body());
            }

            @Override
            public void onFailure(Call<List<Attraction>> call, Throwable t) {
                view.onLoadAttractionsError();
            }

        });
    }

}
