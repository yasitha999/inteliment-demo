package com.yasitha.inteliment.model;

import com.yasitha.inteliment.model.domain.Attraction;
import com.yasitha.inteliment.model.rest.AttractionsAPIClient;
import com.yasitha.inteliment.util.Constant;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yasitha on 6/28/16.
 */
public class AttractionsModel {

    private Retrofit retrofit;

    public AttractionsModel() {

        retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    public Call<List<Attraction>> getAttractions() {
        AttractionsAPIClient attractionsAPIClient = retrofit.create(AttractionsAPIClient.class);
        return attractionsAPIClient.getAttractions();
    }
}
