package com.yasitha.inteliment.model.rest;

import com.yasitha.inteliment.model.domain.Attraction;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Yasitha on 6/28/16.
 */
public interface AttractionsAPIClient {

    @GET("/sample.json")
    Call<List<Attraction>> getAttractions();

}
