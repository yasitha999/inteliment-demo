package com.yasitha.inteliment.model.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Yasitha on 6/28/16.
 */
public class AttractionLocation implements Parcelable {

    private double latitude;
    private double longitude;

    protected AttractionLocation(Parcel in) {
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AttractionLocation> CREATOR = new Creator<AttractionLocation>() {
        @Override
        public AttractionLocation createFromParcel(Parcel in) {
            return new AttractionLocation(in);
        }

        @Override
        public AttractionLocation[] newArray(int size) {
            return new AttractionLocation[size];
        }
    };

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
