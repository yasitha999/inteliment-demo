package com.yasitha.inteliment.model.domain;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yasitha on 6/28/16.
 */
public class Attraction implements Parcelable {

    private long id;

    private String name;

    @SerializedName("fromcentral")
    private Map<String, String> distanceFromCentral = new HashMap<>();

    @SerializedName("location")
    private AttractionLocation attractionLocation;


    protected Attraction(Parcel in) {
        id = in.readLong();
        name = in.readString();
        attractionLocation = in.readParcelable(AttractionLocation.class.getClassLoader());

        int size = in.readInt();
        for(int i = 0; i < size; i++){
            String key = in.readString();
            String value = in.readString();
            distanceFromCentral.put(key,value);
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeLong(id);
        dest.writeString(name);
        dest.writeParcelable(attractionLocation, flags);

        dest.writeInt(distanceFromCentral.size());
        for(Map.Entry<String,String> entry : distanceFromCentral.entrySet()){
            dest.writeString(entry.getKey());
            dest.writeString(entry.getValue());
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Attraction> CREATOR = new Creator<Attraction>() {
        @Override
        public Attraction createFromParcel(Parcel in) {
            return new Attraction(in);
        }

        @Override
        public Attraction[] newArray(int size) {
            return new Attraction[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setDistanceFromCentral(Map<String, String> distanceFromCentral) {
        this.distanceFromCentral = distanceFromCentral;
    }

    public Map<String, String> getDistanceFromCentral() {
        return distanceFromCentral;
    }

    public AttractionLocation getAttractionLocation() {
        return attractionLocation;
    }

    public void setAttractionLocation(AttractionLocation attractionLocation) {
        this.attractionLocation = attractionLocation;
    }

    @Override
    public String toString() {
        return name;
    }
}
